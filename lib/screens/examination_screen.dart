import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hitchmed_demo_web/getX/controller.dart';
import 'package:hitchmed_demo_web/widgets/common/title_contianer.dart';

import '../styles.dart';

class ExaminationScreen extends StatefulWidget {
  @override
  _ExaminationScreenState createState() => _ExaminationScreenState();
}

class _ExaminationScreenState extends State<ExaminationScreen> {
  ScrollController _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<Controller>(
      builder: (getController){
        return Container(
          height: double.infinity,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Color(0xfff5f5f5),
              boxShadow: [
                BoxShadow(
                    spreadRadius: 2,
                    blurRadius: 3,
                    offset: Offset(0,6),
                    color: shadowGery
                )
              ]
          ),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TitleContainer(title: '문진표'),
               Expanded(
                  child: ListView.builder(
                    controller: _scrollController,
                    itemCount: getController.examinationList.length,
                    itemBuilder: (context, index) {
                      return getController.examinationList[index];
                    },
                  ),
                )
              ],
            ),
        );
      }
    );
  }
}
