import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:hitchmed_demo_web/api_connection.dart';
import 'package:hitchmed_demo_web/getX/controller.dart';
import 'package:hitchmed_demo_web/models/question.dart';
import 'package:hitchmed_demo_web/styles.dart';
import 'package:hitchmed_demo_web/widgets/common/title_contianer.dart';
import 'package:hitchmed_demo_web/widgets/self_examination_screen/choice_language_box.dart';
import 'package:hitchmed_demo_web/widgets/self_examination_screen/message.dart';

class SelfExaminationScreen extends StatefulWidget {
  @override
  _SelfExaminationScreenState createState() => _SelfExaminationScreenState();
}

class _SelfExaminationScreenState extends State<SelfExaminationScreen> {
  TextEditingController _textEditingController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  FocusNode focusNode = FocusNode();

  moveScrollToBottom() {
    SchedulerBinding.instance?.addPostFrameCallback((timeStamp) {
      _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: Duration(milliseconds: 100),
          curve: Curves.easeInOut);
    });
  }

  autoKeyboardFocus(){
    FocusScope.of(context).requestFocus(focusNode);
  }

  skipAnswer({required int questionId, required String uuid, required String lang, required String answer}){
    setState(() {
      postUserMessage(questionId: questionId, uuid: uuid, lang: lang, answer: answer);
    });
  }

  Future<void> postUserMessage({required int questionId, required String uuid, required String lang, required String answer}) async {
    await RestApi(url: putAnswerUrl + questionId.toString(),
        body: {"uuid": uuid, "answer": answer, "language": lang, "answerTime": DateTime.now().millisecondsSinceEpoch}).fetchData()
        .then((value) {
          getReQuestion(questionId: questionId, uuid: uuid, language: lang);
    });
  }

  Future<void> getReQuestion({required questionId, required String uuid, required String language}) async {
    Question response;
    await RestApi(url: getQuestionUrl + questionId.toString(),
        body: { "uuid": uuid, "language": language}).fetchData().then((value) {
          if(value.statusCode !=204){
            response = Question.fromJson(jsonDecode(utf8.decode(value.bodyBytes)));
            Controller.to.updateQuestionId(response.id);
            Future.delayed(Duration(milliseconds: 100), (){
              Controller.to.addMessage(ChatbotMessage( question: response.question, isFirst:false, onSkip: skipAnswer,));
              moveScrollToBottom();
            });
            return response;
          }else{
            Controller.to.addMessage(ChoiceLanguageBox(title: '문진표 확인',));
            moveScrollToBottom();
          }
      });
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }


    @override
    Widget build(BuildContext context) {
      return GetBuilder<Controller>(
          builder: (getController) {
            return Container(
                height: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xfff5f5f5),
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: 2,
                          blurRadius: 3,
                          offset: Offset(0, 6),
                          color: shadowGery
                      )
                    ]
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TitleContainer(title: '자가문진'),
                    Expanded(
                      child: ListView.builder(
                        controller: _scrollController,
                        itemCount: getController.messageList.length,
                        itemBuilder: (context, index) {
                          return getController.messageList[index];
                        },
                      ),
                    ),
                    Container(
                        margin: const EdgeInsets.only(top: 10),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 8),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: TextField(
                          focusNode: focusNode,
                          autofocus: true,
                          controller: _textEditingController,
                          decoration: InputDecoration(
                              labelText: '답변을 입력해주세요!',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: const Color(
                                      0xff2bacc8),)
                              ),
                              suffixIcon: IconButton(
                                icon: Icon(Icons.send),
                                onPressed: () {
                                  if(_textEditingController.value.text!='')setState(() {
                                    getController.messageList.add(
                                        UserMessage(message: _textEditingController.value.text,));
                                    _textEditingController.clear();
                                    autoKeyboardFocus();
                                    postUserMessage(
                                        questionId: getController.questionId,
                                        uuid: getController.uuid,
                                        lang: getController.language,
                                        answer: _textEditingController.value.text,);
                                    moveScrollToBottom();
                                  });
                                },
                              )
                          ),
                          onSubmitted: (value) {
                            if(value !='')setState(() {
                              getController.messageList.add(
                                  UserMessage(message: value,));
                              _textEditingController.clear();
                              autoKeyboardFocus();
                              postUserMessage(
                                questionId: getController.questionId,
                                uuid: getController.uuid,
                                lang: getController.language,
                                answer: value);
                            });
                          },
                        )
                    ),
                  ],
                )
            );
          }
      );
    }
  }
