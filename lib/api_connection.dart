import 'dart:convert';
import 'package:http/http.dart' as http;

const API_URL = 'http://114.108.173.106:20701';

const selectLanguageUrl = '/selectLanguage';
const getQuestionUrl = '/getQuestion/';
const putAnswerUrl = '/putAnswer/';
const putQuestionnaireUrl = '/putQuestionnaire';
const getQuestionnaireUrl = '/getQuestionnaire';
const getLogUrl = '/getLog';

class RestApi {
  final Map<String, String> header={
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*", // Required for CORS support to work
    "Accept": "application/json",
    "Access-Control-Allow-Headers": "Access-Control-Allow-Origin, Accept"
  };
  late String url;
  late Map<String, dynamic> body;

  RestApi({ required this.url, required this.body});


  Future<http.Response> fetchData() async{
    final response = await http.post(
      Uri.parse(API_URL + url),
      headers: header,
      body: jsonEncode(body),
    );
    return response;
    }
  }