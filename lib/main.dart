import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hitchmed_demo_web/screens/examination_log_screen.dart';
import 'package:hitchmed_demo_web/screens/examination_screen.dart';
import 'package:hitchmed_demo_web/screens/self_examination_screen.dart';

import 'getX/controller.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'hitchmed',
      home: MainPage(title: ''),
    );
  }
}

class MainPage extends StatefulWidget {
  MainPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  @override
  Widget build(BuildContext context) {
    Get.put(Controller());
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(flex:1,child: SelfExaminationScreen()),
              SizedBox(width: 30,),
              Flexible(flex:1, child: ExaminationScreen()),
              SizedBox(width: 30,),
              Flexible(flex:2, child: ExaminationLogScreen())
            ],
          ),
        ),
      ),
    );
  }
}
