class Question {
  final int id;
  final int diseaseId;
  final String question;

  Question({required this.id, required this.diseaseId, required this.question});

  factory Question.fromJson(Map<String, dynamic> json){
    return Question(
      id: json['question']['id'],
      diseaseId: json['question']['diseaseId'],
      question: json['question']['question']
    );
  }
}