class Questionnaire {
  final List<QuestionnaireItem> questionnaire;

  Questionnaire({required this.questionnaire});

  factory Questionnaire.fromJson(Map<String, dynamic> json){
    List<QuestionnaireItem> questionnaire = [];

    for(int i=0; i<json['questionnaire'].length; i++){
      questionnaire.add(QuestionnaireItem.fromJson(json['questionnaire'][i]));
    }

    return Questionnaire(
      questionnaire: questionnaire
    );
  }
}

class QuestionnaireItem{
  final String clinic;
  final dynamic clinicAnswer;

  QuestionnaireItem({required this.clinic, required this.clinicAnswer});

  factory QuestionnaireItem.fromJson(Map<String, dynamic> json){
    return QuestionnaireItem(
      clinic: json['clinic'],
      clinicAnswer: json['clinicAnswer']
    );
  }

}