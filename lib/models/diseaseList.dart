class DiseaseList {
  final List<DiseaseListItem> diseaseList;
  final String uuid;

  DiseaseList({required this.diseaseList, required this.uuid});

  factory DiseaseList.fromJson(Map<String, dynamic> json){
    List<DiseaseListItem> diseaseList = [];

    diseaseList.add(DiseaseListItem.fromJson({"id" : 0, "name":''}));

    for(int i=0; i<json['diseaseList'].length; i++){
      diseaseList.add(DiseaseListItem.fromJson(json['diseaseList'][i]));
    }

    return DiseaseList(
      diseaseList: diseaseList,
      uuid: json['uuid']
    );
  }
}

class DiseaseListItem{
  final int id;
  final String name;

  DiseaseListItem({required this.id, required this.name});

  factory DiseaseListItem.fromJson(Map<String, dynamic>json){
    return DiseaseListItem(
      id: json['id'],
      name: json['name']
    );
  }
}