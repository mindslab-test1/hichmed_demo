class LogList {
  final List<LogItem> log;

  LogList({required this.log});

  factory LogList.fromJson(Map<String, dynamic> json){
    List<LogItem> log = [];

    for(int i=0; i<json['log'].length; i++){
      log.add(LogItem.fromJson(json['log'][i]));
    }

    return LogList(
      log: log
    );
  }
}

class LogItem {
  final int conversationId;
  final String question;
  final String questionTime;
  final String answer;
  final String answerTime;
  final ClinicLogItem clinicLog;
  final List<NerLogItem>nerLog;

  LogItem(
      {required this.conversationId,
      required this.question,
      required this.questionTime,
      required this.answer,
      required this.answerTime,
      required this.clinicLog,
      required this.nerLog,
      });

  factory LogItem.fromJson(Map<String, dynamic> json){
    List<NerLogItem> nerLog = [];

    if(json['nerLog']!=null){
      for(int i=0; i<json['nerLog'].length; i++){
        nerLog.add(NerLogItem.fromJson(json['nerLog'][i]));
      }
    }

    return LogItem(
        conversationId: json['conversationId'],
        question: json['question'],
        questionTime: json['questionTime'],
        answer: json['answer'],
        answerTime: json['answerTime'],
        clinicLog: ClinicLogItem.fromJson(json['clinicLog']),
        nerLog: nerLog
    );
  }
}

class ClinicLogItem {
   String? itcInput;
   String? itcOutput;
   String? clinicalCode;
   String? textKorClinic;
   String? textKorAnswerClinic;
   String? textEngClinic;
   String? textEngAnswerClinic;
   String? textChnClinic;
   String? textChnAnswerClinic;
   String? textEmrClinic;
   String? textEmrAnswerClinic;
   String? nerYn;

  ClinicLogItem(
      {this.itcInput,
      this.itcOutput,
      this.clinicalCode,
      this.textKorClinic,
      this.textKorAnswerClinic,
      this.textEngClinic,
      this.textEngAnswerClinic,
      this.textChnClinic,
      this.textChnAnswerClinic,
      this.textEmrClinic,
      this.textEmrAnswerClinic,
      this.nerYn});

  factory ClinicLogItem.fromJson(Map<String, dynamic> json){
    return ClinicLogItem(
        itcInput : json['itcInput'],
        itcOutput : json['itcOutput'],
        clinicalCode : json['clinicalCode'],
        textKorClinic : json['textKorClinic'],
        textKorAnswerClinic : json['textKorAnswerClinic'],
        textEngClinic : json['textEngClinic'],
        textEngAnswerClinic : json['textEngAnswerClinic'],
        textChnClinic : json['textChnClinicc'],
        textChnAnswerClinic : json['textChnAnswerClinic'],
        textEmrClinic : json['textEmrClinic'],
        textEmrAnswerClinic : json['textEmrAnswerClinic'],
        nerYn : json['nerYn']
    );
  }
}

class NerLogItem {
  String? nerInput;
  String? nerOutput;
  String? entityKor;
  String? entityEng;
  String? entityChn;
  String? entityEmr;
  String? nerTextKor;
  String? nerTextEng;
  String? nerTextChn;

  NerLogItem({
    this.nerInput,
    this.nerOutput,
    this.entityKor,
    this.entityEng,
    this.entityChn,
    this.entityEmr,
    this.nerTextKor,
    this.nerTextEng,
    this.nerTextChn
  });

  factory NerLogItem.fromJson(Map<String, dynamic> json){
    return NerLogItem(
        nerInput : json['nerInput'],
        nerOutput : json['nerOutput'],
        entityKor : json['entityKor'],
        entityEng : json['entityEng'],
        entityChn : json['entityChn'],
        entityEmr : json['entityEmr'],
        nerTextKor : json['nerTextKor'],
        nerTextEng : json['nerTextEng'],
        nerTextChn : json['nerTextChn']
    );
  }

}

