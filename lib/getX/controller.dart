import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hitchmed_demo_web/widgets/self_examination_screen/choice_language_box.dart';

class Controller extends GetxController {
  static Controller get to=> Get.find();

  late String language;
  late bool isLangSelected;
  late String uuid;
  late int questionId;
  late String examinationLang;
  List<Widget> messageList = [ChoiceLanguageBox(title: '언어선택',)];
  List<Widget> examinationList = [];
  List<Widget> logList = [];

  updateLanguage(String lang){
    language = lang;
    update();
  }

  updateIsLangSelected(bool isSelected){
    isLangSelected = isSelected;
    update();
  }

  addMessage(Widget widget){
    messageList.add(widget);
    update();
  }

  updateUuid(String userUuid){
    uuid = userUuid;
    update();
  }

  updateQuestionId(int inputId){
    questionId = inputId;
    update();
  }

  updateExaminationLang(String choiceLang){
    examinationLang = choiceLang;
    update();
  }

  addExaminationList(Widget widget){
    examinationList.add(widget);
    update();
  }

  removeExaminationLoading(){
    examinationList.clear();
    update();
  }

  addLogList(Widget widget){
    logList.add(widget);
    update();
  }
}