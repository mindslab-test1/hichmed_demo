import 'package:flutter/cupertino.dart';

// color
const shadowGery = Color(0xffe2e2e2);

// text style
const titleTextStyle = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
const subTitleTextStyle = TextStyle(fontWeight: FontWeight.w400, fontSize: 16);