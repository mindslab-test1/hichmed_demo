import 'package:flutter/material.dart';
import 'package:hitchmed_demo_web/styles.dart';

class TitleContainer extends StatelessWidget {
  final String title;

  const TitleContainer({required this.title});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(20),
        topRight: Radius.circular(20)
      ),
      child: Container(
        height: 100,
        color: const Color(0xff2bacc8),
        padding: const EdgeInsets.symmetric(vertical: 28),
        child: Center(
          child: Text(title, style: titleTextStyle.copyWith(color: Colors.white),),
        ),
      ),
    );
  }
}
