import 'package:flutter/material.dart';

class LogItemText extends StatelessWidget {
  final String title;
  final String answer;
  final bool isBold;
  const LogItemText({Key? key, required this.title, required this.answer, required this.isBold}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
     child: Text(title + ' : ' + answer, style : TextStyle(fontWeight: isBold?FontWeight.bold:FontWeight.normal)),
    );
  }
}
