import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hitchmed_demo_web/getX/controller.dart';
class MessageList extends StatefulWidget {
  late List<Widget> messageList;
  MessageList({Key? key, required this.messageList}) : super(key: key);

  @override
  _MessageListState createState() => _MessageListState();
}

class _MessageListState extends State<MessageList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Flexible(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: widget.messageList
        ),
      ),
    );
  }
}

class ChatbotMessage extends StatelessWidget {
  late String question;
  late bool isFirst;
  Function? onSkip;
  bool isSkipped = false;
  bool isBacked = false;

  ChatbotMessage({Key? key, required this.question, required this.isFirst, this.onSkip,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<Controller>(
      builder: (getController){
        return Container(
          margin: const EdgeInsets.only(top: 20, left: 20,),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 50),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.zero,
                        topRight: Radius.circular(20),
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)
                    ),
                    border: Border.all(
                        color: Colors.black,
                        width: 1
                    )
                ),
                child: Text(question),
              ),
              SizedBox(height: 5,),
              Row(
                children: [
                  if(!isFirst)TextButton(
                    onPressed: (){
                      if(!isBacked){
                        getController.messageList.removeLast();
                        Controller.to.updateQuestionId(getController.questionId-1);
                      }
                      isBacked = true;
                    },
                    child: Text('back'),
                  ),
                  if(onSkip !=null)TextButton(
                    onPressed: isSkipped?null:(){
                        onSkip!(
                            lang: getController.language,
                            questionId: getController.questionId,
                            uuid : getController.uuid,
                            answer : ''
                        );
                        isSkipped = true;
                    },
                    child: Text('skip'),
                  )
                ],
              )
            ],
          ),
        );
      },
    );
  }
}

class UserMessage extends StatelessWidget {
  late String message;
  UserMessage({Key? key, required this.message,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 20, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 50),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.zero,
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20)
                  ),
                  border: Border.all(
                      color: Colors.black,
                      width: 1
                  )
              ),
              child: Text(message),
            ),
          ],
        ),
    );
  }
}