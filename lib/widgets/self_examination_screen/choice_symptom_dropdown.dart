import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hitchmed_demo_web/api_connection.dart';
import 'package:hitchmed_demo_web/getX/controller.dart';
import 'package:hitchmed_demo_web/models/diseaseList.dart';
import 'package:hitchmed_demo_web/models/question.dart';
import 'package:hitchmed_demo_web/styles.dart';
import 'package:hitchmed_demo_web/widgets/self_examination_screen/message.dart';

class SymptomDropDown extends StatefulWidget {
  const SymptomDropDown({Key? key}) : super(key: key);

  @override
  _SymptomDropDownState createState() => _SymptomDropDownState();
}

class _SymptomDropDownState extends State<SymptomDropDown> with AutomaticKeepAliveClientMixin{
  String selectedValue='';
  late DiseaseList diseaseList;
  bool isClicked = false;

  Future<DiseaseList> getDiseaseList(String lang) async {
    DiseaseList response = await RestApi(url: selectLanguageUrl, body: {"language" : lang}).fetchData().then((value)
      =>DiseaseList.fromJson(jsonDecode(utf8.decode(value.bodyBytes)))
    );
    return response;
  }

  Future<Question> getQuestion({required String uuid, required String language}) async{
    Question response;
    response = await RestApi(url: getQuestionUrl,
        body: {"uuid" : uuid, "language": language}).fetchData().then((value)
         => Question.fromJson(jsonDecode(utf8.decode(value.bodyBytes))));
    Controller.to.updateQuestionId(response.id);
    Controller.to.addMessage(ChatbotMessage(question: response.question, isFirst: true,));
    return response;
  }

  searchInfo({required List<DiseaseListItem> loadList, required String name, required String uuid}){
    loadList.forEach((element) {
      if(element.name==name){
        getQuestion(uuid: uuid, language: Get.find<Controller>().language);
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getDiseaseList(Get.find<Controller>().language),
      builder: (BuildContext context, AsyncSnapshot snapshot){
        if(snapshot.hasData){
          List<DiseaseListItem> diseaseList = snapshot.data.diseaseList;
          String uuid = snapshot.data.uuid;
          return Container(
            width: double.infinity,
            margin: const EdgeInsets.all(20),
            padding: const EdgeInsets.only(top: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color(0xffe5e5e5),
            ),
            child: Column(
              children: [
                Center(child: Text('주호소', style: subTitleTextStyle,)),
                DropdownButton(
                  disabledHint: Text(selectedValue),
                  value:selectedValue,
                  underline: SizedBox(),
                  items: diseaseList.map((value){
                    return DropdownMenuItem(
                      value: value.name,
                      child: Text(value.name),
                    );
                  }).toList(),
                  onChanged: !isClicked?(value){
                    setState(() {
                      selectedValue = value.toString();
                      isClicked = true;
                      searchInfo(loadList: diseaseList, name: value.toString(), uuid: uuid);
                      Controller.to.updateUuid(uuid);
                    });
                  }:null,
                ),
              ],
            )
          );
        }else{
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
