import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hitchmed_demo_web/api_connection.dart';
import 'package:hitchmed_demo_web/getX/controller.dart';
import 'package:hitchmed_demo_web/models/diseaseList.dart';
import 'package:hitchmed_demo_web/models/logList.dart';
import 'package:hitchmed_demo_web/models/question.dart';
import 'package:hitchmed_demo_web/models/questionnaire.dart';
import 'package:hitchmed_demo_web/styles.dart';
import 'package:hitchmed_demo_web/widgets/common/custom_indicator.dart';
import 'package:hitchmed_demo_web/widgets/examination/answerListItem.dart';
import 'package:hitchmed_demo_web/widgets/examination_log/logItemText.dart';
import 'package:hitchmed_demo_web/widgets/self_examination_screen/message.dart';

class ChoiceLanguageBox extends StatefulWidget {
  final title;

  const ChoiceLanguageBox({Key? key, this.title}) : super(key: key);
  @override
  _ChoiceLanguageBoxState createState() => _ChoiceLanguageBoxState();
}

class _ChoiceLanguageBoxState extends State<ChoiceLanguageBox> with AutomaticKeepAliveClientMixin{
  List<String> langList = ['한국어', '영어', '중국어', 'EMR'];
  List<String> langCodeList = ['KOR', 'ENG', 'CHN', 'EMR'];
  int selectedIndex = -1;
  bool isClicked = false;

  Future<void> setLanguage(String lang) async {
    DiseaseList response = await RestApi(url: selectLanguageUrl, body: {"language" : lang}).fetchData().then((value)
    =>DiseaseList.fromJson(jsonDecode(utf8.decode(value.bodyBytes)))
    );
    Controller.to.updateUuid(response.uuid);
    getQuestion(uuid: response.uuid, language: lang);
  }

  Future<Question> getQuestion({required String uuid, required String language}) async{
    Question response;
    response = await RestApi(url: getQuestionUrl,
        body: {"uuid" : uuid, "language": language}).fetchData().then((value)
    => Question.fromJson(jsonDecode(utf8.decode(value.bodyBytes))));
    Controller.to.updateQuestionId(response.id);
    Controller.to.addMessage(ChatbotMessage(question: response.question, isFirst: true,));
    return response;
  }

  Future<void> putExamination() async{
    Controller.to.removeExaminationLoading();
    Controller.to.addExaminationList(CustomIndicator());
    await RestApi(url: putQuestionnaireUrl,
        body: {"uuid" : Get.find<Controller>().uuid}
        ).fetchData().then((value){
          Controller.to.removeExaminationLoading();
     getExamination();
    });
  }

  Future<void> getExamination() async {
    await RestApi(url: getQuestionnaireUrl,
        body: {"uuid" : Get.find<Controller>().uuid, "language" : Get.find<Controller>().examinationLang}
    ).fetchData().then((value){
      Questionnaire questionnaire = Questionnaire.fromJson(jsonDecode(utf8.decode(value.bodyBytes)));
      addExaminationList(questionnaire: questionnaire.questionnaire);
      if(Get.find<Controller>().logList.length==0)getLog();
    });
  }

  Future<void> getLog() async {
    await RestApi(url: getLogUrl, body: {"uuid": Get.find<Controller>().uuid}).fetchData().then((value){
      LogList logList = LogList.fromJson(jsonDecode(utf8.decode(value.bodyBytes)));
      addLogList(logList: logList);
    });
  }

  addExaminationList({required List<QuestionnaireItem> questionnaire}){
    questionnaire.forEach((element) {
      Controller.to.addExaminationList(AnswerListItem(question: element.clinic, answer: element.clinicAnswer));
    });
  }

  addLogList({required LogList logList}){
    // lang
    Controller.to.addLogList(LogItemText(title: 'lang' , answer :Get.find<Controller>().language, isBold: false,));
    Controller.to.addLogList(SizedBox(height: 20));

    // question and answer
    logList.log.forEach((element) {
      Controller.to.addLogList(LogItemText(title: 'conversationId', answer : element.conversationId.toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'question', answer : element.question, isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'questionTime', answer :DateTime.parse(element.questionTime.toString()).toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'answer', answer : element.answer, isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'answerTime', answer : DateTime.parse(element.answerTime.toString()).toString(), isBold: false,));
      Controller.to.addLogList(SizedBox(height: 20));
    });

    //itc and ner
    logList.log.forEach((element) {
      Controller.to.addLogList(LogItemText(title: 'conversationId', answer : element.conversationId.toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'itcInput', answer : element.clinicLog.itcInput.toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'itcOutput', answer : element.clinicLog.itcOutput.toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'clinicalCode', answer : element.clinicLog.clinicalCode.toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'textKorClinic', answer : element.clinicLog.textKorClinic.toString(), isBold: true,));
      Controller.to.addLogList(LogItemText(title: 'textKorAnswerClinic', answer : element.clinicLog.textKorAnswerClinic.toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'textEngClinic', answer : element.clinicLog.textEngClinic.toString(), isBold: true,));
      Controller.to.addLogList(LogItemText(title: 'textEngAnswerClinic', answer : element.clinicLog.textEngAnswerClinic.toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'textChnClinic', answer : element.clinicLog.textChnClinic.toString(), isBold: true,));
      Controller.to.addLogList(LogItemText(title: 'textChnAnswerClinic', answer : element.clinicLog.textChnAnswerClinic.toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'textEmrClinic', answer : element.clinicLog.textEmrClinic.toString(), isBold: true,));
      Controller.to.addLogList(LogItemText(title: 'textEmrAnswerClinic', answer : element.clinicLog.textEmrAnswerClinic.toString(), isBold: false,));
      Controller.to.addLogList(LogItemText(title: 'nerYn', answer : element.clinicLog.nerYn.toString(), isBold: false,));
      element.nerLog.forEach((log) {
        Controller.to.addLogList(LogItemText(title:'nerInput',answer : log.nerInput.toString(), isBold: false,));
        Controller.to.addLogList(LogItemText(title:'nerOutput',answer : log.nerOutput.toString(), isBold: false,));
        Controller.to.addLogList(LogItemText(title:'entityKor',answer : log.entityKor.toString(), isBold: false,));
        Controller.to.addLogList(LogItemText(title:'entityEng',answer : log.entityEng.toString(), isBold: false,));
        Controller.to.addLogList(LogItemText(title:'entityChn',answer : log.entityChn.toString(), isBold: false,));
        Controller.to.addLogList(LogItemText(title:'entityEmr',answer : log.entityEmr.toString(), isBold: false,));
        Controller.to.addLogList(LogItemText(title:'nerTextKor',answer : log.nerTextKor.toString(), isBold: false,));
        Controller.to.addLogList(LogItemText(title:'nerTextEng',answer : log.nerTextEng.toString(), isBold: false,));
        Controller.to.addLogList(LogItemText(title:'nerTextChn',answer : log.nerTextChn.toString(), isBold: false,));
      });
    });
  }

  @override
  void initState() {
    if(widget.title=='언어선택'){
      langList = langList.sublist(0,langList.length-1);
      langCodeList = langCodeList.sublist(0, langCodeList.length-1);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.all(20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: const Color(0xffe5e5e5),
        ),
        child: Column(
          children: [
            Container(
                padding: const EdgeInsets.only(top: 10),
                child: Center(
                    child: Text(widget.title,
                  style: subTitleTextStyle,
                ))),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              child: GridView.builder(
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: widget.title=='언어선택'?3 : 2,
                    childAspectRatio: 3 / 1,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10),
                itemCount: langList.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          backgroundColor: selectedIndex==index?Color(0xff185663):Colors.transparent,
                        ),
                        onPressed: () {
                          if(widget.title =='언어선택'){
                            if(!isClicked)setState(() {
                              selectedIndex = index;
                              setLanguage(langCodeList[index]);
                              Controller.to.updateLanguage(langCodeList[index]);
                              Controller.to.updateIsLangSelected(true);
                              isClicked = true;
                            });
                          }else{
                            setState(() {
                              selectedIndex = index;
                              isClicked = true;
                              Controller.to.updateExaminationLang(langCodeList[index]);
                              putExamination();
                            });
                          }
                        },
                        child: Text(langList[index],
                          style: TextStyle(color: selectedIndex==index?Colors.white:Color(0xff222222),
                              fontWeight: selectedIndex==index?FontWeight.bold:FontWeight.normal),),
                      ));
                },
              ),
            )
          ],
        ));
  }

  @override
  bool get wantKeepAlive => true;
}
