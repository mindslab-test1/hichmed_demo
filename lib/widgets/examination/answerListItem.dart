import 'package:flutter/material.dart';

class AnswerListItem extends StatefulWidget {
  late String question;
  late dynamic answer;
  AnswerListItem({Key? key, required this.question, required this.answer}) : super(key: key);

  @override
  _AnswerListItemState createState() => _AnswerListItemState();
}

class _AnswerListItemState extends State<AnswerListItem> {
  List<Widget> answers = [];

  @override
  void initState() {
    super.initState();
    if(widget.answer is String){
      answers.add(Text(widget.answer));
    }else{
      widget.answer.forEach((element){
        answers.add(Text(element));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      padding: const EdgeInsets.only(left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.question, style: TextStyle(fontWeight: FontWeight.bold),),
          SizedBox(height: 10,),
          ...answers
        ],
      ),
    );
  }
}
